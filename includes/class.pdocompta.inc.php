<?php

/**
 * Classe d'accÃ¨s aux donnÃ©es.
 *
 * Utilise les services de la classe PDO
 * pour l'application GSB
 * Les attributs sont tous statiques,
 * les 4 premiers pour la connexion
 * $monPdo de type PDO
 * $monPdoCompta qui contiendra l'unique instance de la classe
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Romain Janz <romainjanz@gmailcom>
 * @copyright 2019 libre
 * @license   libre
 * @version   Release: 1.0
 * @link      http://www.php.net/manual/fr/book.pdo.php PHP Data Objects sur php.net
 */


class pdoCompta
{
    private static $serveur = 'mysql:host=localhost';
    private static $bdd = 'dbname=gsb_frais';
    private static $user = 'userGsb';
    private static $mdp = 'secret';
    private static $monPdo;
    private static $monpdoCompta = null;

    /**
     * Constructeur privÃ©, crÃ©e l'instance de PDO qui sera sollicitÃ©e
     * pour toutes les mÃ©thodes de la classe
     */
    private function __construct()
    {
        pdoCompta::$monPdo = new PDO(
            pdoCompta::$serveur . ';' . pdoCompta::$bdd,
            pdoCompta::$user,
            pdoCompta::$mdp
        );
        pdoCompta::$monPdo->query('SET CHARACTER SET utf8');
    }

    /**
     * MÃ©thode destructeur appelÃ©e dÃ¨s qu'il n'y a plus de rÃ©fÃ©rence sur un
     * objet donnÃ©, ou dans n'importe quel ordre pendant la sÃ©quence d'arrÃªt.
     */
    public function __destruct()
    {
        pdoCompta::$monPdo = null;
    }

    /**
     * Fonction statique qui crÃ©e l'unique instance de la classe
     * Appel : $instancepdoCompta = pdoCompta::getpdoCompta();
     *
     * @return l'unique objet de la classe pdoCompta
     */
    public static function getpdoCompta()
    {
        if (pdoCompta::$monpdoCompta == null) {
            pdoCompta::$monpdoCompta = new pdoCompta();
        }
        return pdoCompta::$monpdoCompta;
    }

    /**
     * Retourne les informations d'un personnel
     *
     * @param String $login Login du personnel
     * @param String $mdp   Mot de passe du personnel
     *
     * @return l'id, le nom et le prÃ©nom sous la forme d'un tableau associatif
     */
    public function getInfosUtilisateur($login, $mdp)
    {
        $requetePrepare = PdoGsb::$monPdo->prepare(
            'SELECT personnel.id AS id, personnel.nom AS nom, '
            . 'personnel.prenom AS prenom, personnel.idemploi AS idemploi '
            . 'FROM personnel JOIN emploi ON personnel.idemploi = emploi.id '
            . 'WHERE personnel.login = :unLogin AND personnel.mdp = :unMdp'
        );
        $requetePrepare->bindParam(':unLogin', $login, PDO::PARAM_STR);
        $requetePrepare->bindParam(':unMdp', $mdp, PDO::PARAM_STR);
        $requetePrepare->execute();
        return $requetePrepare->fetch();
    }

    /**
     * Retourne les informations d'un visiteur
     *
     * @return  le nom et le prÃ©nom sous la forme d'un tableau associatif
     */
    public function getUnVisiteur($idVisiteur)
    {
        $requetePrepare = pdoCompta::$monPdo->prepare(
            'SELECT personnel.nom AS nom, '
            . 'personnel.prenom AS prenom '
            . 'FROM personnel '
            . 'WHERE personnel.id = :idVisiteur '
            );
        $requetePrepare->bindParam(':idVisiteur', $idVisiteur, PDO::PARAM_STR);
        $requetePrepare->execute();
        return $requetePrepare->fetch();
    }
    
    /**
     * Retourne tous les visteurs
     *
     * @return l'id, le nom et le prÃ©nom sous la forme d'un tableau associatif
     */
    public function getAllLesVisiteursCloture()
    {
        $requetePrepare = pdoCompta::$monPdo->prepare(
            'SELECT personnel.id AS id, '
            . 'personnel.prenom AS prenom, '
            . 'personnel.nom AS nom '
            . 'FROM personnel '
            . 'ORDER BY personnel.id asc'
            );
        $requetePrepare->execute();
        $lesVisiteursMois = array();
        while ($laLigne = $requetePrepare->fetch()) {
            $lesVisiteurs[] = array(
                'id' => $laLigne['id'],
                'prenom' => $laLigne['prenom'],
                'nom' => $laLigne['nom']
            );
        }
        return $lesVisiteurs;
    }
    
    /**
     * Retourne tous les visteurs qui ont une fiche de frais cloturer ou créer
     *
     * @return l'id, le nom et le prÃ©nom sous la forme d'un tableau associatif
     */
    public function getLesVisiteursCloture($uc)
    {
        if($uc == 'suivrePayement')
        {
            $premEtat = 'VA';
            $deusEtat = 'RB';
        }
        if($uc == 'validerFrais')
        {
            $premEtat = 'CL';
            $deusEtat = 'VA';
        }
        $requetePrepare = pdoCompta::$monPdo->prepare(
            'SELECT personnel.id AS id, '
            . 'personnel.prenom AS prenom, '
            . 'personnel.nom AS nom '
            . 'FROM personnel '
            . 'WHERE id = ANY (SELECT personnel.id '
            .     'FROM personnel INNER JOIN fichefrais '
            .     'ON (personnel.id = fichefrais.idpersonnel) '
            .     "WHERE fichefrais.idetat = :premEtat OR  fichefrais.idetat = :deusEtat) "
            . 'ORDER BY personnel.id asc'
            );
        $requetePrepare->bindParam(':premEtat', $premEtat, PDO::PARAM_STR);
        $requetePrepare->bindParam(':deusEtat', $deusEtat, PDO::PARAM_STR);
        $requetePrepare->execute();
        $lesVisiteursMois = array();
        while ($laLigne = $requetePrepare->fetch()) {
            $lesVisiteurs[] = array(
                'id' => $laLigne['id'],
                'prenom' => $laLigne['prenom'],
                'nom' => $laLigne['nom']
            );
        }
        return $lesVisiteurs;
    }
    
    /**
     * Retourne les mois pour lesquel un visiteur a une fiche de frais cloturer ou valider
     * @param unknown $id   id du visiteur
     * @return un tableau associatif de clÃ© un mois -aaaamm- et de valeurs l'annÃ©e et le mois correspondant
     */
    public function getLesMoisDisponiblesCL($uc, $id)
    {
        if($uc == 'suivrePayement'){
            $premEtat = 'VA';
            $deusEtat = 'RB';
        }
        if($uc == 'validerFrais')
        {
            $premEtat = 'CL';
            $deusEtat = 'VA';
        }
        $requetePrepare = pdoCompta::$monPdo->prepare(
            'SELECT fichefrais.idpersonnel AS id, '
            . 'fichefrais.mois AS mois '
            . 'FROM fichefrais '
            . "WHERE fichefrais.idpersonnel = :unId "
            . "AND (fichefrais.idetat = :premEtat OR  fichefrais.idetat = :deusEtat) "
            . 'ORDER BY fichefrais.idpersonnel, fichefrais.mois desc '
            );
        $requetePrepare->bindParam(':unId', $id, PDO::PARAM_STR);
        $requetePrepare->bindParam(':premEtat', $premEtat, PDO::PARAM_STR);
        $requetePrepare->bindParam(':deusEtat', $deusEtat, PDO::PARAM_STR);
        $requetePrepare->execute();
        $lesVisiteursMois = array();
        while ($laLigne = $requetePrepare->fetch()) {
            $mois = $laLigne['mois'];
            $numAnnee = substr($mois, 0, 4);
            $numMois = substr($mois, 4, 2);
            $lesVisiteursMois[] = array(
                'mois' => $mois,
                'numAnnee' => $numAnnee,
                'numMois' => $numMois
            );
            
        }
        return $lesVisiteursMois;
    }
    
    /**
     * modifie le mois au mois suivant un frais hors forfait et rajoute au libille 'REFUSER' 
     * @param unknown $mois        Mois du frais hors forfait
     * @param unknown $idFrais     ID du frais hors forfait
     */
    public function majRefuserFraisHorsForfait($mois, $idFrais)
    {
        $requetePrepare = pdoCompta::$monPdo->prepare(
            'UPDATE `lignefraishorsforfait` '
            . 'SET lignefraishorsforfait.mois = :unMois, '
            . "lignefraishorsforfait.libelle = CONCAT('REFUSER ', lignefraishorsforfait.libelle ) "
            . 'WHERE lignefraishorsforfait.id = :idFrais '
            );
        $requetePrepare->bindParam(':unMois', $mois, PDO::PARAM_STR);
        $requetePrepare->bindParam(':idFrais', $idFrais, PDO::PARAM_STR);
        $requetePrepare->execute();
    }
    
    /**
    * Modifie la date, le montant et libelle d'un frais hors forfait
    * @param unknown $idFrais       ID du frais hors forfait
    * @param unknown $date          date du frais hors forfait
    * @param unknown $montant       montant du frais hors forfait
    * @param unknown $libelle       libelle du frais hors forfait
    */
    public function majFraisHorsForfait( $idFrais, $date, $montant, $libelle)
    {
        $dateFr = dateFrancaisVersAnglais($date);
        $requetePrepare = pdoCompta::$monPdo->prepare(
            'UPDATE `lignefraishorsforfait` '
            . 'SET lignefraishorsforfait.libelle = :unLibelle, lignefraishorsforfait.date = :uneDateFr, lignefraishorsforfait.montant = :unMontant '
            . 'WHERE lignefraishorsforfait.id = :idFrais '
            );
       
        $requetePrepare->bindParam(':idFrais', $idFrais, PDO::PARAM_STR);
        $requetePrepare->bindParam(':unLibelle', $libelle, PDO::PARAM_STR);
        $requetePrepare->bindParam(':uneDateFr', $dateFr, PDO::PARAM_STR);
        $requetePrepare->bindParam(':unMontant', $montant, PDO::PARAM_INT);
        $requetePrepare->execute();
    }
    
    /**
     * Retourne tous les id de la table FraisForfait
     * @return unknown
     */
    public function getLesFrais()
    {
        $requetePrepare = pdoCompta::$monPdo->prepare(
            'SELECT fraisforfait.id as idfrais, '
            . 'fraisforfait.montant as montant '
            . 'FROM fraisforfait ORDER BY fraisforfait.id'
            );
        $requetePrepare->execute();
        
        return $requetePrepare->fetchAll();
    }
    
    
    
    /**
     * 
     * @param string $idVisiteur
     * @param unknown $date
     * @param unknown $montantValide
     */
    public function majValiderFicheFrais( $idVisiteur, $leMois, $montantValide, $nbJustificatifs)
    {
        $requetePrepare = pdoCompta::$monPdo->prepare(
            'UPDATE `fichefrais` '
            . "SET fichefrais.idetat ='VA', fichefrais.montantValide = :montantValide , fichefrais.nbJustificatifs = :nbJustificatifs "
            . 'WHERE fichefrais.idpersonnel = :idVisiteur '
            . 'AND fichefrais.mois = :leMois '
            );
        $requetePrepare->bindParam(':idVisiteur', $idVisiteur, PDO::PARAM_STR);
        $requetePrepare->bindParam(':leMois', $leMois, PDO::PARAM_INT);
        $requetePrepare->bindParam(':montantValide', $montantValide, PDO::PARAM_INT); 
        $requetePrepare->bindParam(':nbJustificatifs', $nbJustificatifs, PDO::PARAM_INT); 
        $requetePrepare->execute();
       
    }
    
    /**
     *
     * @param string $idVisiteur
     * @param unknown $date
     * @param unknown $montantValide
     */
    public function majRembourserFicheFrais( $idVisiteur, $leMois)
    {
        $requetePrepare = pdoCompta::$monPdo->prepare(
            'UPDATE `fichefrais` '
            . "SET fichefrais.idetat ='RB' "
            . 'WHERE fichefrais.idpersonnel = :idVisiteur '
            . 'AND fichefrais.mois = :leMois '
            );
        $requetePrepare->bindParam(':idVisiteur', $idVisiteur, PDO::PARAM_STR);
        $requetePrepare->bindParam(':leMois', $leMois, PDO::PARAM_INT);
        $requetePrepare->execute();
        
    }
    
    public function majDateModifIdPerso($idVisiteur, $mois, $comptable)
    {
        $requetePrepare = pdoCompta::$monPdo->prepare(
            'UPDATE `fichefrais` '
            . "SET fichefrais.datemodif = CURDATE() , fichefrais.idpersomodif = :comptable "
            . 'WHERE fichefrais.idpersonnel = :idVisiteur '
            . 'AND fichefrais.mois = :leMois '
            );
        $requetePrepare->bindParam(':idVisiteur', $idVisiteur, PDO::PARAM_STR);
        $requetePrepare->bindParam(':leMois', $mois, PDO::PARAM_INT);
        $requetePrepare->bindParam(':comptable', $comptable, PDO::PARAM_STR);
        $requetePrepare->execute();
    }
    
}

