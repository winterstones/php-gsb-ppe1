<?php
/**
 * Classe d'accÃ¨s aux donnÃ©es.
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Cheri Bibi - RÃ©seau CERTA <contact@reseaucerta.org>
 * @author    JosÃ© GIL - CNED <jgil@ac-nice.fr>
 * @copyright 2017 RÃ©seau CERTA
 * @license   RÃ©seau CERTA
 * @version   GIT: <0>
 * @link      http://www.php.net/manual/fr/book.pdo.php PHP Data Objects sur php.net
 */

/**
 * Classe d'accÃ¨s aux donnÃ©es.
 *
 * Utilise les services de la classe PDO
 * pour l'application GSB
 * Les attributs sont tous statiques,
 * les 4 premiers pour la connexion
 * $monPdo de type PDO
 * $monPdoGsb qui contiendra l'unique instance de la classe
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Cheri Bibi - RÃ©seau CERTA <contact@reseaucerta.org>
 * @author    JosÃ© GIL <jgil@ac-nice.fr>
 * @copyright 2017 RÃ©seau CERTA
 * @license   RÃ©seau CERTA
 * @version   Release: 1.0
 * @link      http://www.php.net/manual/fr/book.pdo.php PHP Data Objects sur php.net
 */

class PdoGsb
{
    private static $serveur = 'mysql:host=localhost';
    private static $bdd = 'dbname=gsb_frais';
    private static $user = 'userGsb';
    private static $mdp = 'secret';
    private static $monPdo;
    private static $monPdoGsb = null;

    /**
     * Constructeur privÃ©, crÃ©e l'instance de PDO qui sera sollicitÃ©e
     * pour toutes les mÃ©thodes de la classe
     */
    private function __construct()
    {
        PdoGsb::$monPdo = new PDO(
            PdoGsb::$serveur . ';' . PdoGsb::$bdd,
            PdoGsb::$user,
            PdoGsb::$mdp
        );
        PdoGsb::$monPdo->query('SET CHARACTER SET utf8');
    }

    /**
     * MÃ©thode destructeur appelÃ©e dÃ¨s qu'il n'y a plus de rÃ©fÃ©rence sur un
     * objet donnÃ©, ou dans n'importe quel ordre pendant la sÃ©quence d'arrÃªt.
     */
    public function __destruct()
    {
        PdoGsb::$monPdo = null;
    }

    /**
     * Fonction statique qui crÃ©e l'unique instance de la classe
     * Appel : $instancePdoGsb = PdoGsb::getPdoGsb();
     *
     * @return l'unique objet de la classe PdoGsb
     */
    public static function getPdoGsb()
    {
        if (PdoGsb::$monPdoGsb == null) {
            PdoGsb::$monPdoGsb = new PdoGsb();
        }
        return PdoGsb::$monPdoGsb;
    }

    /**
     * Retourne les informations d'un personnel
     *
     * @param String $login Login du personnel
     * @param String $mdp   Mot de passe du personnel
     *
     * @return l'id, le nom et le prÃ©nom sous la forme d'un tableau associatif
     */
    public function getInfosUtilisateur($login, $mdp)
    {
        $requetePrepare = PdoGsb::$monPdo->prepare(
            'SELECT personnel.id AS id, personnel.nom AS nom, '
            . 'personnel.prenom AS prenom, personnel.idemploi AS idemploi ,secteur.libelle AS secteur'
            . 'FROM personnel JOIN emploi ON personnel.idemploi = emploi.id'
            .'JOIN secteur ON personnel.idsecteur = secteur.id'
            . 'WHERE personnel.login = :unLogin AND personnel.mdp = :unMdp'
        );
        $requetePrepare->bindParam(':unLogin', $login, PDO::PARAM_STR);
        $requetePrepare->bindParam(':unMdp', $mdp, PDO::PARAM_STR);
        $requetePrepare->execute();
        print_r($requetePrepare->fetch());
        return $requetePrepare->fetch();
    }
    
    /**
     * Retourne sous forme d'un tableau associatif toutes les lignes de frais
     * hors forfait concernÃ©es par les deux arguments.
     * La boucle foreach ne peut Ãªtre utilisÃ©e ici car on procÃ¨de
     * Ã  une modification de la structure itÃ©rÃ©e - transformation du champ date-
     *
     * @param String $idpersonnel ID du personnel
     * @param String $mois       Mois sous la forme aaaamm
     *
     * @return tous les champs des lignes de frais hors forfait sous la forme
     * d'un tableau associatif
     */
    public function getLesFraisHorsForfait($idpersonnel, $mois)
    {
        $requetePrepare = PdoGsb::$monPdo->prepare(
            'SELECT * FROM lignefraishorsforfait '
            . 'WHERE lignefraishorsforfait.idpersonnel = :unIdpersonnel '
            . 'AND lignefraishorsforfait.mois = :unMois'
        );
        $requetePrepare->bindParam(':unIdpersonnel', $idpersonnel, PDO::PARAM_STR);
        $requetePrepare->bindParam(':unMois', $mois, PDO::PARAM_STR);
        $requetePrepare->execute();
        $lesLignes = $requetePrepare->fetchAll();
        for ($i = 0; $i < count($lesLignes); $i++) {
            $date = $lesLignes[$i]['date'];
            $lesLignes[$i]['date'] = dateAnglaisVersFrancais($date);
            if ($lesLignes[$i]['refuser'] == 1){
                $lesLignes[$i]['refuser'] = 'refusé';
            }else {
                $lesLignes[$i]['refuser'] = 'accepté';
            }
        }
        return $lesLignes;
    }

    /**
     * Retourne le nombre de justificatif d'un personnel pour un mois donnÃ©
     *
     * @param String $idpersonnel ID du personnel
     * @param String $mois       Mois sous la forme aaaamm
     *
     * @return le nombre entier de justificatifs
     */
    public function getNbjustificatifs($idpersonnel, $mois)
    {
        $requetePrepare = PdoGsb::$monPdo->prepare(
            'SELECT fichefrais.nbjustificatifs as nb FROM fichefrais '
            . 'WHERE fichefrais.idpersonnel = :unIdpersonnel '
            . 'AND fichefrais.mois = :unMois'
        );
        $requetePrepare->bindParam(':unIdpersonnel', $idpersonnel, PDO::PARAM_STR);
        $requetePrepare->bindParam(':unMois', $mois, PDO::PARAM_STR);
        $requetePrepare->execute();
        $laLigne = $requetePrepare->fetch();
        return $laLigne['nb'];
    }

    /**
     * Retourne sous forme d'un tableau associatif toutes les lignes de frais
     * au forfait concernÃ©es par les deux arguments
     *
     * @param String $idpersonnel ID du personnel
     * @param String $mois       Mois sous la forme aaaamm
     *
     * @return l'id, le libelle et la quantitÃ© sous la forme d'un tableau
     * associatif
     */
    public function getLesFraisForfait($idpersonnel, $mois)
    {
        $requetePrepare = PdoGsb::$monPdo->prepare(
            'SELECT fraisforfait.id as idfrais, '
            . 'fraisforfait.libelle as libelle, '
            . 'lignefraisforfait.quantite as quantite '
            . 'FROM lignefraisforfait '
            . 'INNER JOIN fraisforfait '
            . 'ON fraisforfait.id = lignefraisforfait.idfraisforfait '
            . 'WHERE lignefraisforfait.idpersonnel = :unIdpersonnel '
            . 'AND lignefraisforfait.mois = :unMois '
            . 'ORDER BY lignefraisforfait.idfraisforfait'
        );
        $requetePrepare->bindParam(':unIdpersonnel', $idpersonnel, PDO::PARAM_STR);
        $requetePrepare->bindParam(':unMois', $mois, PDO::PARAM_STR);
        $requetePrepare->execute();
        return $requetePrepare->fetchAll();
    }

    /**
     * Retourne tous les id de la table FraisForfait
     *
     * @return un tableau associatif
     */
    public function getLesIdFrais()
    {
        $requetePrepare = PdoGsb::$monPdo->prepare(
            'SELECT fraisforfait.id as idfrais '
            . 'FROM fraisforfait ORDER BY fraisforfait.id'
        );
        $requetePrepare->execute();
        return $requetePrepare->fetchAll();
    }

    /**
     * Met Ã  jour la table ligneFraisForfait
     * Met Ã  jour la table ligneFraisForfait pour un personnel et
     * un mois donnÃ© en enregistrant les nouveaux montants
     *
     * @param String $idpersonnel ID du personnel
     * @param String $mois       Mois sous la forme aaaamm
     * @param Array  $lesFrais   tableau associatif de clÃ© idFrais et
     *                           de valeur la quantitÃ© pour ce frais
     *
     * @return null
     */
    public function majFraisForfait($idpersonnel, $mois, $lesFrais)
    {
        $lesCles = array_keys($lesFrais);
        foreach ($lesCles as $unIdFrais) {
            $qte = $lesFrais[$unIdFrais];
            $requetePrepare = PdoGsb::$monPdo->prepare(
                'UPDATE lignefraisforfait '
                . 'SET lignefraisforfait.quantite = :uneQte '
                . 'WHERE lignefraisforfait.idpersonnel = :unIdpersonnel '
                . 'AND lignefraisforfait.mois = :unMois '
                . 'AND lignefraisforfait.idfraisforfait = :idFrais'
            );
            $requetePrepare->bindParam(':uneQte', $qte, PDO::PARAM_INT);
            $requetePrepare->bindParam(':unIdpersonnel', $idpersonnel, PDO::PARAM_STR);
            $requetePrepare->bindParam(':unMois', $mois, PDO::PARAM_STR);
            $requetePrepare->bindParam(':idFrais', $unIdFrais, PDO::PARAM_STR);
            $requetePrepare->execute();
        }
    }

    /**
     * Met Ã  jour le nombre de justificatifs de la table ficheFrais
     * pour le mois et le personnel concernÃ©
     *
     * @param String  $idpersonnel      ID du personnel
     * @param String  $mois            Mois sous la forme aaaamm
     * @param Integer $nbJustificatifs Nombre de justificatifs
     *
     * @return null
     */
    public function majNbJustificatifs($idpersonnel, $mois, $nbJustificatifs)
    {
        $requetePrepare = PdoGsb::$monPdo->prepare(
            'UPDATE fichefrais '
            . 'SET nbjustificatifs = :unNbJustificatifs '
            . 'WHERE fichefrais.idpersonnel = :unIdpersonnel '
            . 'AND fichefrais.mois = :unMois'
        );
        $requetePrepare->bindParam(
            ':unNbJustificatifs',
            $nbJustificatifs,
            PDO::PARAM_INT
        );
        $requetePrepare->bindParam(':unIdpersonnel', $idpersonnel, PDO::PARAM_STR);
        $requetePrepare->bindParam(':unMois', $mois, PDO::PARAM_STR);
        $requetePrepare->execute();
    }

    /**
     * Teste si un personnel possÃ¨de une fiche de frais pour le mois passÃ© en argument
     *
     * @param String $idpersonnel ID du personnel
     * @param String $mois       Mois sous la forme aaaamm
     *
     * @return vrai ou faux
     */
    public function estPremierFraisMois($idpersonnel, $mois)
    {
        $boolReturn = false;
        $requetePrepare = PdoGsb::$monPdo->prepare(
            'SELECT fichefrais.mois FROM fichefrais '
            . 'WHERE fichefrais.mois = :unMois '
            . 'AND fichefrais.idpersonnel = :unIdpersonnel'
        );
        $requetePrepare->bindParam(':unMois', $mois, PDO::PARAM_STR);
        $requetePrepare->bindParam(':unIdpersonnel', $idpersonnel, PDO::PARAM_STR);
        $requetePrepare->execute();
        if (!$requetePrepare->fetch()) {
            $boolReturn = true;
        }
        return $boolReturn;
    }

    /**
     * Retourne le dernier mois en cours d'un personnel
     *
     * @param String $idpersonnel ID du personnel
     *
     * @return le mois sous la forme aaaamm
     */
    public function dernierMoisSaisi($idpersonnel)
    {
        $requetePrepare = PdoGsb::$monPdo->prepare(
            'SELECT MAX(mois) as dernierMois '
            . 'FROM fichefrais '
            . 'WHERE fichefrais.idpersonnel = :unIdpersonnel'
        );
        $requetePrepare->bindParam(':unIdpersonnel', $idpersonnel, PDO::PARAM_STR);
        $requetePrepare->execute();
        $laLigne = $requetePrepare->fetch();
        $dernierMois = $laLigne['dernierMois'];
        return $dernierMois;
    }

    /**
     * CrÃ©e une nouvelle fiche de frais et les lignes de frais au forfait
     * pour un personnel et un mois donnÃ©s
     *
     * RÃ©cupÃ¨re le dernier mois en cours de traitement, met Ã  'CL' son champs
     * idEtat, crÃ©e une nouvelle fiche de frais avec un idEtat Ã  'CR' et crÃ©e
     * les lignes de frais forfait de quantitÃ©s nulles
     *
     * @param String $idpersonnel ID du personnel
     * @param String $mois       Mois sous la forme aaaamm
     *
     * @return null
     */
    public function creeNouvellesLignesFrais($idpersonnel, $mois)
    {
        $dernierMois = $this->dernierMoisSaisi($idpersonnel);
        $laDerniereFiche = $this->getLesInfosFicheFrais($idpersonnel, $dernierMois);
        if ($laDerniereFiche['idEtat'] == 'CR') {
            $this->majEtatFicheFrais($idpersonnel, $dernierMois, 'CL');
        }
        $requetePrepare = PdoGsb::$monPdo->prepare(
            'INSERT INTO fichefrais (idpersonnel,mois,nbjustificatifs,'
            . 'montantvalide,datemodif,idetat) '
            . "VALUES (:unIdpersonnel,:unMois,0,0,now(),'CR')"
        );
        $requetePrepare->bindParam(':unIdpersonnel', $idpersonnel, PDO::PARAM_STR);
        $requetePrepare->bindParam(':unMois', $mois, PDO::PARAM_STR);
        $requetePrepare->execute();
        $lesIdFrais = $this->getLesIdFrais();
        foreach ($lesIdFrais as $unIdFrais) {
            $requetePrepare = PdoGsb::$monPdo->prepare(
                'INSERT INTO lignefraisforfait (idpersonnel,mois,'
                . 'idfraisforfait,quantite) '
                . 'VALUES(:unIdpersonnel, :unMois, :idFrais, 0)'
            );
            $requetePrepare->bindParam(':unIdpersonnel', $idpersonnel, PDO::PARAM_STR);
            $requetePrepare->bindParam(':unMois', $mois, PDO::PARAM_STR);
            $requetePrepare->bindParam(
                ':idFrais',
                $unIdFrais['idfrais'],
                PDO::PARAM_STR
            );
            $requetePrepare->execute();
        }
    }

    /**
     * CrÃ©e un nouveau frais hors forfait pour un personnel un mois donnÃ©
     * Ã  partir des informations fournies en paramÃ¨tre
     *
     * @param String $idpersonnel ID du personnel
     * @param String $mois       Mois sous la forme aaaamm
     * @param String $libelle    LibellÃ© du frais
     * @param String $date       Date du frais au format franÃ§ais jj//mm/aaaa
     * @param Float  $montant    Montant du frais
     *
     * @return null
     */
    public function creeNouveauFraisHorsForfait(
        $idpersonnel,
        $mois,
        $libelle,
        $date,
        $montant
    ) {
        $dateFr = dateFrancaisVersAnglais($date);
        $requetePrepare = PdoGsb::$monPdo->prepare(
            'INSERT INTO lignefraishorsforfait '
            . 'VALUES (null, :unIdpersonnel,:unMois, :unLibelle, :uneDateFr,'
            . ':unMontant) '
        );
        $requetePrepare->bindParam(':unIdpersonnel', $idpersonnel, PDO::PARAM_STR);
        $requetePrepare->bindParam(':unMois', $mois, PDO::PARAM_STR);
        $requetePrepare->bindParam(':unLibelle', $libelle, PDO::PARAM_STR);
        $requetePrepare->bindParam(':uneDateFr', $dateFr, PDO::PARAM_STR);
        $requetePrepare->bindParam(':unMontant', $montant, PDO::PARAM_INT);
        $requetePrepare->execute();
    }

    /**
     * Supprime le frais hors forfait dont l'id est passÃ© en argument
     *
     * @param String $idFrais ID du frais
     *
     * @return null
     */
    public function supprimerFraisHorsForfait($idFrais)
    {
        $requetePrepare = PdoGsb::$monPdo->prepare(
            'DELETE FROM lignefraishorsforfait '
            . 'WHERE lignefraishorsforfait.id = :unIdFrais'
        );
        $requetePrepare->bindParam(':unIdFrais', $idFrais, PDO::PARAM_STR);
        $requetePrepare->execute();
    }

    /**
     * Retourne les mois pour lesquel un personnel a une fiche de frais
     *
     * @param String $idpersonnel ID du personnel
     *
     * @return un tableau associatif de clÃ© un mois -aaaamm- et de valeurs
     *         l'annÃ©e et le mois correspondant
     */
    public function getLesMoisDisponibles($idpersonnel)
    {
        $requetePrepare = PdoGsb::$monPdo->prepare(
            'SELECT fichefrais.mois AS mois FROM fichefrais '
            . "WHERE fichefrais.idpersonnel = :unIdpersonnel AND ficheFrais.idetat != 'CR'"
            . 'ORDER BY fichefrais.mois desc'
        );
        $requetePrepare->bindParam(':unIdpersonnel', $idpersonnel, PDO::PARAM_STR);
        $requetePrepare->execute();
        $lesMois = array();
        while ($laLigne = $requetePrepare->fetch()) {
            $mois = $laLigne['mois'];
            $numAnnee = substr($mois, 0, 4);
            $numMois = substr($mois, 4, 2);
            $lesMois[] = array(
                'mois' => $mois,
                'numAnnee' => $numAnnee,
                'numMois' => $numMois
            );
        }
        return $lesMois;
    }

    /**
     * Retourne les informations d'une fiche de frais d'un personnel pour un
     * mois donnÃ©
     *
     * @param String $idpersonnel ID du personnel
     * @param String $mois       Mois sous la forme aaaamm
     *
     * @return un tableau avec des champs de jointure entre une fiche de frais
     *         et la ligne d'Ã©tat
     */
    public function getLesInfosFicheFrais($idpersonnel, $mois)
    {
        $requetePrepare = PdoGsb::$monPdo->prepare(
            'SELECT fichefrais.idetat as idEtat, '
            . 'fichefrais.datemodif as dateModif,'
            . 'fichefrais.nbjustificatifs as nbJustificatifs, '
            . 'fichefrais.montantvalide as montantValide, '
            . 'etat.libelle as libEtat '
            . 'FROM fichefrais '
            . 'INNER JOIN etat ON fichefrais.idetat = etat.id '
            . 'WHERE fichefrais.idpersonnel = :unIdpersonnel '
            . 'AND fichefrais.mois = :unMois'
        );
        $requetePrepare->bindParam(':unIdpersonnel', $idpersonnel, PDO::PARAM_STR);
        $requetePrepare->bindParam(':unMois', $mois, PDO::PARAM_STR);
        $requetePrepare->execute();
        $laLigne = $requetePrepare->fetch();
        return $laLigne;
    }

    /**
     * Modifie l'Ã©tat et la date de modification d'une fiche de frais.
     * Modifie le champ idEtat et met la date de modif Ã  aujourd'hui.
     *
     * @param String $idpersonnel ID du personnel
     * @param String $mois       Mois sous la forme aaaamm
     * @param String $etat       Nouvel Ã©tat de la fiche de frais
     *
     * @return null
     */
    public function majEtatFicheFrais($idpersonnel, $mois, $etat)
    {
        $requetePrepare = PdoGsb::$monPdo->prepare(
            'UPDATE ficheFrais '
            . 'SET idetat = :unEtat, datemodif = now() '
            . 'WHERE fichefrais.idpersonnel = :unIdpersonnel '
            . 'AND fichefrais.mois = :unMois'
        );
        $requetePrepare->bindParam(':unEtat', $etat, PDO::PARAM_STR);
        $requetePrepare->bindParam(':unIdpersonnel', $idpersonnel, PDO::PARAM_STR);
        $requetePrepare->bindParam(':unMois', $mois, PDO::PARAM_STR);
        $requetePrepare->execute();
    }
}
?>
    
    