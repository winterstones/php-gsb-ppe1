<?php
    session_start();
    require_once '../includes/class.pdogsb.inc.php'; 
    require_once '../includes/fct.inc.php';
	
	$pdo = PdoGsb::getPdoGsb();
	
    $data = json_decode(file_get_contents('php://input'),true);
    $idFrais = $data['id'];
	echo json_encode($idFrais);
	$pdo->supprimerFraisHorsForfait($idFrais);