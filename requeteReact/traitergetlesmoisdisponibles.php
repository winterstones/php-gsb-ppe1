<?php
    session_start();
    require_once '../includes/class.pdogsb.inc.php'; 
    require_once '../includes/fct.inc.php';
	$pdo = PdoGsb::getPdoGsb();
	
	$data = json_decode(file_get_contents('php://input'),true);
    $idVisiteur = $data['id'];
	
	$lesMoisDisponibles = $pdo->getLesMoisDisponibles($idVisiteur);
	
	if(isset($lesMoisDisponibles)){
		echo json_encode($lesMoisDisponibles);
	}else{
		echo json_encode("erreur, il n'y a pas de fiche de frais disponible");
	}