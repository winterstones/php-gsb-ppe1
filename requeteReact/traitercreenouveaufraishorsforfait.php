<?php
    session_start();
    require_once '../includes/class.pdogsb.inc.php'; 
    require_once '../includes/fct.inc.php';
	$pdo = PdoGsb::getPdoGsb();
    $data = json_decode(file_get_contents('php://input'),true);
    $idVisiteur = $data['id'];
	$libelle = $data['libelle'];
	$dateFrais = $data['dateFrais'];
	$montant = $data['montant'];

    $mois = getMois(date('d/m/Y'));
    $numAnnee = substr($mois, 0, 4);
    $numMois = substr($mois, 4, 2);
	
	valideInfosFrais($dateFrais, $libelle, $montant);
    if (nbErreurs() != 0) {
		//foreach ($_REQUEST['erreurs'] as $erreur) {
        //echo json_encode(htmlspecialchars($erreur));
		//}
		echo json_encode($_REQUEST['erreurs']);
    } else {
        $pdo->creeNouveauFraisHorsForfait(
            $idVisiteur,
            $mois,
            $libelle,
            $dateFrais,
            $montant
        );
		echo json_encode('Nouveau frais hors forfait ajouté');
    };
	
	//$lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $mois);
	