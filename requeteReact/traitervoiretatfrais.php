<?php
    session_start();
    require_once '../includes/class.pdogsb.inc.php'; 
    require_once '../includes/fct.inc.php';
	$pdo = PdoGsb::getPdoGsb();
	
	$data = json_decode(file_get_contents('php://input'),true);
    $idVisiteur = $data['id'];
	$leMois = $data['leMois'];
	
	$lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $leMois);
    $lesFraisForfait = $pdo->getLesFraisForfait($idVisiteur, $leMois);
    $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($idVisiteur, $leMois);
	
	$laFicheFrais = array('forfait' => $lesFraisForfait, 'horsForfait' => $lesFraisHorsForfait, 'infos' => $lesInfosFicheFrais);
	echo json_encode($laFicheFrais);
?>