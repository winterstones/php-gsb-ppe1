<?php
 session_start();
 require_once '../includes/class.pdogsb.inc.php'; 
 $data = json_decode(file_get_contents('php://input'),true);
 $login = $data['login'];
 $mdp = $data['mdp'];
 $pdo = PdoGsb::getPdoGsb();
 $visiteur = array();
 $visiteur = $pdo->getInfosVisiteur($login,$mdp);
 if($visiteur != null){
     $_SESSION['visiteur'] = $visiteur;
     $_SESSION['visiteur']['login'] = $login;
     $_SESSION['visiteur']['mdp'] = $mdp;
     $visiteur = json_encode($visiteur);
	 echo $visiteur;
 }else{
	 echo json_encode('fail');
 }

  
?>
