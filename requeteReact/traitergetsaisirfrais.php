<?php
    session_start();
    require_once '../includes/class.pdogsb.inc.php'; 
    require_once '../includes/fct.inc.php';
    $data = json_decode(file_get_contents('php://input'),true);
    $idVisiteur = $data['id'];
    $pdo = PdoGsb::getPdoGsb();
    $mois = getMois(date('d/m/Y'));
    $numAnnee = substr($mois, 0, 4);
    $numMois = substr($mois, 4, 2);
    if ($pdo->estPremierFraisMois($idVisiteur, $mois)) {
        $pdo->creeNouvellesLignesFrais($idVisiteur, $mois);
    }
    $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $mois);
    $lesFraisForfait = $pdo->getLesFraisForfait($idVisiteur, $mois);
    $lesFrais = array('forfait' => $lesFraisForfait, 'horsForfait' => $lesFraisHorsForfait);
    echo json_encode($lesFrais);
?>