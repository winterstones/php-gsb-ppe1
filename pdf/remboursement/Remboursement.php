<?php
require '../../includes/fct.inc.php';
require 'fpdf181/fpdf.php';

/**
 * Classe paermettent de generer un fiche de remenboursement en PDF
 * @author pc
 *
 */
class Remboursement extends \FPDF{
    private $visiteur;          //Nom du visiteur
    private $montantTotal;      //Montant total
    private $date;              //Date d'édition de la facture
    private $referance;         //Reference de la facture
    //Ligne des nuitees
    private $nuitee = array(
        "quantite" => 0,
        "montant" => 0,
        "total"=> 0
    );
    private $repas = array(
        "quantite" => 0,
        "montant" => 0,
        "total"=> 0
    );
    private $etape = array(
        "quantite" => 0,
        "montant" => 0,
        "total"=> 0
    );
    private $kilomettre = array(
        "quantite" => 0,
        "montant" => 0,
        "total"=> 0
    );
    //Ligne des frais hors forfait
    private $ligneFraisHorsFrais = array(
        "date" => '',
        "libelle"=> '',
        "montant" => ''
    );
    private $j = 0;
    private $lesFraisHorsFrais = array();   //Tableau qui recevra l'ensemble des lignes des frais hors forfait
    
    /**
     * Construction de la classe Remboursement qui hérite de la FPDF
     * * initialisation de l'auteur, du titre, du sujet
     */
    public function construct(){
        //création du document
        $pdf = new FPDF('P','mm','A4');
        $this->SetAuthor('Romain wint');
        $this->SetCreator('generer avec FPCF1.81' . phpversion());
        $this->SetTitle('Remboursement des frais engages');
        $this->SetSubject('Remboursement des frais engages');
    }
  
    /**
     * Setter des frais forfait repas
     *
     * @param int $quantite Quantité de repas
     * @param reel $montant Montant du repas
     * @param reel $total Calcul total des repas
      */
    public function setRepas($quantite, $montant, $total) {
        $this->repas["quantite"] = $quantite;
        $this->repas["montant"] = $montant;
        $this->repas["total"] = $total;
    }
       
    /**
     * Setter du montant total
     *
     * @param reel $montantTotal Montant total de la fiche de frais
     */
    public function setMontantTotal($montantTotal) {
        $this->montantTotal = $montantTotal;
    }
           
   /**
    * Setter des frais forfait nuitee
    *
    * @param int $quantite Quantité de nuitee
    * @param reel $montant Montant du nuitee
    * @param reel $total Calcul total des nuitees
    */
    public function setNuitee($quantite, $montant, $total) {
        $this->nuitee["quantite"] = $quantite;
        $this->nuitee["montant"] = $montant;
        $this->nuitee["total"] = $total;
    }
    /**
     * Setter des frais forfait etape
     *
     * @param int $quantite Quantité de nuitee
     * @param reel $montant Montant du nuitee
     * @param reel $total Calcul total des nuitees
     */
    public function setEtape($quantite, $montant, $total) {
        $this->nuitee["quantite"] = $quantite;
        $this->nuitee["montant"] = $montant;
        $this->nuitee["total"] = $total;
    }
    /**
     * Setter des frais forfait km
     *
     * @param int $quantite Quantité de km
     * @param reel $montant Montant du km
     * @param reel $total Calcul total des kms
     */
    public function setKilomettre($quantite, $montant, $total) {
        $this->kilomettre["quantite"] = $quantite;
        $this->kilomettre["montant"] = $montant;
        $this->kilomettre["total"] = $total;
    }
        
    /**
     * Permet d'ajouter à la fiche un frais
     *
     * @param date $date Date du frais
     * @param string $libelle Libelle du frais
     * @param reel $montant Montant du frais
     */
    public function AjouterFraisHorsFrais($date, $libelle, $montant) {
        $this->lesFraisHorsFrais[] = array($date, $libelle, $montant);
        $this->j++;
    }
    
    public function Afficher() {
        $this->SetX(30);
        $this->SetFont('Times', 'B', 10);
        $this->Cell(20, 10, 'Visiteur :', 0, 0, 'c', false);
        $this->SetFont('Times', '', 10);
        $this->Cell(30, 10, $this->visiteur, 0, 0, 'c', false);
        $this->SetFont('Times', 'B', 10);
        $this->Cell(30, 10, utf8_decode('Référence :'), 0, 0, 'C', false);
        $this->SetFont('Times', '', 10);
        $this->Cell(30, 10, $this->reference, 0, 0, 'L', false);
        $this->SetFont('Times', 'B', 10);
        $this->Cell(20, 10, 'Mois :', 0, 0, 'C', false);
        $this->SetFont('Times', '', 10);
        $this->Cell(30, 10, $this->date, 0, 0, 'L', false);
        $this->Ln(15);
        
        $this->SetFillColor(125, 159, 205);
        $this->Cell(70, 10, "Frais forfaitaires", 'BTL', 0, 'C', true);
        $this->Cell(30, 10, "Quantite", 'BT', 0, 'C', true);
        $this->Cell(30, 10, "Montant", 'BT', 0, 'C', true);
        $this->Cell(30, 10, "Total", 'BTR', 1, 'C', true);
        
        $this->Cell(50, 10, utf8_decode("Nuitée"), 1, 0, 'C', false);
        $this->Cell(30, 10, $this->nuitee['quantite'], 1, 0, 'C', false);
        $this->Cell(30, 10, $this->nuitee['montant'], 1, 0, 'C', false);
        $this->Cell(30, 10, $this->nuitee['total'], 1, 1, 'C', false);
        
        $this->SetFillColor(209, 216, 220);
        $this->Cell(50, 10, "Repas midi", 1, 0, 'C', true);
        $this->Cell(30, 10, $this->repas['quantite'], 1, 0, 'C', true);
        $this->Cell(30, 10, $this->repas['montant'], 1, 0, 'C', true);
        $this->Cell(30, 10, $this->repas['total'], 1, 1, 'C', true);
        
        $this->Cell(50, 10, utf8_decode('Frais Kilométrique'), 1, 0, 'C', false);
        $this->Cell(30, 10, $this->kilomettre['quantite'], 1, 0, 'C', false);
        $this->Cell(30, 10, $this->kilomettre['montant'], 1, 0, 'C', false);
        $this->Cell(30, 10, $this->kilomettre['total'], 1, 1, 'C', false);
        
        $this->SetFillColor(209, 216, 220);
        $this->Cell(50, 10, "Etape", 1, 0, 'C', true);
        $this->Cell(30, 10, $this->etape['quantite'], 1, 0, 'C', true);
        $this->Cell(30, 10, $this->etape['montant'], 1, 0, 'C', true);
        $this->Cell(30, 10, $this->etape['total'], 1, 1, 'C', true);
        
        $total = $this->etape['total'] + $this->kilomettre['total'] + $this->repas['total'] + $this->nuitee['total'];
        
        $this->SetFillColor(125, 159, 205);
        $this->Cell(40, 10, 'Frais Hors Forfait', 0, 0, 'C', false);
        $this->SetX(125);
        $this->Cell(30, 10, $total, 1, 1, 'C', false);
        $this->Cell(30, 10, "Date", 'BTL', 0, 'C', true);
        $this->Cell(80, 10, "Libelle", 'BT', 0, 'C', true);
        $this->Cell(30, 10, "Montant", 'BTR', 1, 'C', true);
        $totalHF = 0;
        $this->SetFillColor(209, 216, 220);
        for ($i = 0; $i < $this->j; $i++) {
            if ($i % 2 == 1) {
                $varLoc = true;
            } else {
                $varLoc = false;
            }
            $this->Cell(30, 10, $this->lesFraisHorsFrais[$i][0], 1, 0, 'C', $varLoc);
            $this->Cell(80, 10, $this->lesFraisHorsFrais[$i][1], 1, 0, 'C', $varLoc);
            $this->Cell(30, 10, $this->lesFraisHorsFrais[$i][2], 1, 1, 'C', $varLoc);
            $totalHF += $this->lesFraisHorsFrais[$i][2];
        }
        $this->SetX(125);
        $this->Cell(30, 10, $totalHF, 1, 1, 'C', false);
        $this->Ln(5);
        $this->SetX(95);
        $this->SetFillColor(125, 159, 205);
        $this->Cell(30, 10, "Montant Total :", 1, 0, 'C', true);
        $this->Cell(30, 10, $this->montantTotal, 1, 1, 'C');
        
        $this->Ln(5);
        $x = $this->GetX();
        $y = $this->GetY();
        $y+=0;
        $x+=90;
        
        $this->SetXY($x, $y);
        $this->Cell(35, 10, utf8_decode("Fais à Paris, le"), 0, 0, 'R', false);
        $this->SetX($this->GetX() - 10);
        $this->Cell(50, 10, strftime('%d %B %Y'), 0, 1, 'R', false);
        $y+=10;
        $x+=0;
        $this->SetXY($x, $y);
        $this->Cell(50, 10, "Vu l'agent comptable", 0, 1, 'R', false);
        $y+=10;
        $x+=0;
        $this->SetXY($x, $y);
        $this->Image('../../images/signature.png');
    }

    /**
     * Génération de l'en-tête
     */
    function Header() {
        
        // Logo
        $this->Image('../../images/logo.jpg', 90, 20, 30);
        // Police Arial gras 15
        $this->SetFont('Times', '', 10);
        // décalalge de droite
         $this->SetXY(10, 45);
        // Titre
        $this->SetFillColor(120, 159, 205);
         $this->Cell(190, 10, utf8_decode('Remboursement de frais engagés'), 1, 1, 'C', true);
        $this->Ln(5);
    }

    /**
     * Affichage du fichier
     *
     * @param sring $name Nom du ficher
     */
    public function AfficherFichier($name) {
        header('Content-Type: application/pdf');
        header('Content-Disposition: inline; filename="' . $name . '"');
        header('Cache-Control: private, max-age=0, must-revalidate');
        header('Pragma: public');
        readfile($name);
    }

    /**
     * Génération du PDF
     *
     * @param string $pNom Nom du visiteur
     * @param date $pDate Date de la fiche de frais
     * @param string $pRef Référence de la fiche de frais
     * @param string $nomPdf Nom du ficher pdf
     */
    public function Generer($pNom, $pDate, $pRef, $nomPdf) {
        
        $this->visiteur = $pNom;
        $this->date = $pDate;
        $this->reference = $pRef;
        $this->AddPage();
        $this->SetMargins(15, 15);
        $this->SetFont('Times', '', 10);
        $this->Header();
        $this->SetAutoPageBreak(TRUE, 30);
        
        $this->Afficher();
        if (file_exists('essai1')) {
            $this->AfficherFichier('essai1');
        } else {
            $this->AfficherFichier($this->Output());
        }
    }

    /**
    * Génération du pied de page
    */
    function Footer() {
        $this->SetY(-15);
        $this->SetFont('Times', 'I', 9);
    }

}
require_once '../../includes/class.pdogsb.inc.php';
require_once '../../includes/class.pdocompta.inc.php';
$pdo = PdoGsb::getPdoGsb();
$pdoc = pdoCompta::getPdoCompta();



$idUser = filter_input(INPUT_GET, 'visiteur', FILTER_SANITIZE_STRING);
$moisSaisi = filter_input(INPUT_GET, 'mois', FILTER_SANITIZE_STRING);

$mois = substr($moisSaisi, 4, 6);
$annee = substr($moisSaisi, 0, 4);
if ($mois > '09') {
    $leMois = $mois . " " . $annee;
} else {
    $mois = substr($mois, 1, 2);
    $leMois = $mois . " " . $annee;
}

$lesFraisVisiteur = $pdo->getLesFraisForfait($idUser, $moisSaisi );

$lgNbEtape = $lesFraisVisiteur[0][2];
$lgNbRepas = $lesFraisVisiteur[3][2];
$lgNbNuitee = $lesFraisVisiteur[2][2];
$lgNbKm =$lesFraisVisiteur[1][2];

$fraisForfais = $pdoc->getLesFrais();

$montantNui = $fraisForfais[2][1];
$montantEtp = $fraisForfais[0][1];
$montantRep = $fraisForfais[3][1];
$montantKm = $fraisForfais[1][1];

// Instanciation de la classe d�riv�e
$pdf = new Remboursement();
$pdf->construct();
$montantTotal = $pdo->getLesInfosFicheFrais($idUser, $moisSaisi);

$pdf->setMontantTotal($montantTotal['montantValide']);
$pdf->setNuitee($lgNbNuitee, $montantNui, $lgNbNuitee * $montantNui);
$pdf->setEtape($lgNbEtape, $montantEtp, $lgNbEtape * $montantEtp);
$pdf->setRepas($lgNbRepas, $montantRep, $lgNbRepas * $montantRep);
$pdf->setKilomettre($lgNbKm, $montantKm, $montantKm * $lgNbKm);

$idJeuEltsHorsForfait = $pdo->getLesFraisHorsForfait($idUser, $moisSaisi);
foreach ($idJeuEltsHorsForfait as $lgEltHorsForfait) {
    $pdf->AjouterFraisHorsFrais($lgEltHorsForfait["date"], utf8_decode($lgEltHorsForfait["libelle"]), $lgEltHorsForfait["montant"]);
}
$lgUser = $pdoc->getUnVisiteur($idUser);
print_r($lgUser);
ob_get_clean(); //pour vider le tampon de sortie

$pdf->AliasNbPages();
$pdf->SetFont('Times', '', 10);
$nom = $lgUser['prenom'];
$prenom = $lgUser['nom'];

$pdf->Generer($nom . " " . $prenom, utf8_decode($leMois), $idUser, "./upload/" . $idUser . "$moisSaisi.pdf");
