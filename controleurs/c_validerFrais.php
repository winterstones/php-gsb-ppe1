<?php
/**
 * Vue Liste des mois
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Romain janz <cndromai@gamil.com>
 * @copyright no
 * @license   no
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */

$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
$idComptable = $_SESSION['id'];

switch ($action) {
    case 'selectionnerVisiteurs':
        /*recuper tous les visiteur de la base de donn� */
        $lesVisiteurs = $pdoc->getLesVisiteursCloture($uc);
        //print_r($lesVisiteurs);
        if(empty($visiteurASelectionner)){
            $visiteurASelectionner = $lesVisiteurs[0]['id'];
        }
        /*recuper tous les mois corespondant au visiteur*/
        $lesMois = $pdoc->getLesMoisDisponiblesCL($uc, $visiteurASelectionner);
        // Afin de sélectionner par défaut le dernier mois dans la zone de liste
        // on demande toutes les clés, et on prend la première,
        // les mois étant triés décroissants
        
        $lesCles = array_keys($lesMois);
        $moisASelectionner = $lesCles[0];
        //print_r($lesMois);
        include 'vues/v_listeVisiteurMois.php';
        break;
    
    case 'selectionnerMois':
        $lesVisiteurs = $pdoc->getLesVisiteursCloture($uc);
        
        $idVisiteur = filter_input(INPUT_POST, 'lstVisiteur', FILTER_SANITIZE_STRING);
        $visiteurASelectionner = $idVisiteur;
        
        /*recuper tous les mois corespondant au visiteur*/
        $lesMois = $pdoc->getLesMoisDisponiblesCL($uc, $idVisiteur);
        // Afin de sélectionner par défaut le dernier mois dans la zone de liste
        // on demande toutes les clés, et on prend la première,
        // les mois étant triés décroissants
        
        $lesCles = array_keys($lesMois);
        $moisASelectionner = $lesCles[0];
        //print_r($lesMois);
        include 'vues/v_listeVisiteurMois.php';
        break;
    case 'tryFraisForfait':
        $lesVisiteurs = $pdoc->getLesVisiteursCloture($uc);
        $visiteur = filter_input(INPUT_POST, 'lstVisiteur2', FILTER_SANITIZE_STRING);
        $visiteurASelectionner = $visiteur;
        $lesMois = $pdoc->getLesMoisDisponiblesCL($uc, $visiteur);
        $leMois = filter_input(INPUT_POST, 'lstMois', FILTER_SANITIZE_STRING);
        $moisASelectionner = $leMois;
        include 'vues/v_listeVisiteurMois.php';
        $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($visiteur, $leMois);
        $lesFraisForfait = $pdo->getLesFraisForfait($visiteur, $leMois);
        $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($visiteur, $leMois);
        $numAnnee = substr($leMois, 0, 4);
        $numMois = substr($leMois, 4, 2);
        $libEtat = $lesInfosFicheFrais['libEtat'];
        $montantValide = $lesInfosFicheFrais['montantValide'];
        $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
        $dateModif = dateAnglaisVersFrancais($lesInfosFicheFrais['dateModif']);
        include 'vues/v_ValiderAll.php';
        break;
    case 'tryFraisForfaitSubmit':
        $montantFrais = $pdoc->getLesFrais();
        //recuperation des formulaire
        $visiteur = filter_input(INPUT_POST, 'idVisiteur', FILTER_SANITIZE_STRING);
        $leMois = filter_input(INPUT_POST, 'mois', FILTER_SANITIZE_STRING);
        $lesFrais = filter_input(INPUT_POST, 'lesFrais', FILTER_DEFAULT, FILTER_FORCE_ARRAY);
        $lesHorsForfait = filter_input(INPUT_POST, 'lesFraisHorsForfait', FILTER_DEFAULT, FILTER_FORCE_ARRAY);
        $nbJustificatifs = filter_input(INPUT_POST, 'justficatifs', FILTER_VALIDATE_INT);
        $requete = filter_input(INPUT_POST, 'requete', FILTER_DEFAULT, FILTER_SANITIZE_STRING);
        $unFraisHorsForfait = filter_input(INPUT_POST, 'unFraisHorsForfait', FILTER_DEFAULT, FILTER_SANITIZE_STRING);
        $suprimerFraisHorsForfait = filter_input(INPUT_POST, 'suprimerFraisHorsForfait', FILTER_DEFAULT, FILTER_SANITIZE_STRING);
        //gestion de la requete
        
        if(!empty($requete)){
            switch ($requete){
                case 'MajFraisForfait':
                    if (lesQteFraisValides($lesFrais)) {
                        $pdo->majFraisForfait($visiteur, $leMois, $lesFrais);
                        ajouterReussite("La modification a été prise en compte");
                        include 'vues/v_success.php';
                    } else {
                        ajouterErreur('Les valeurs des frais doivent être numériques');
                        include 'vues/v_erreurs.php';
                    }
                    break;
                case 'MajFicheForfait':
                    $totalF = 0;
                    $totalHF = 0;
                    $erreur = false;
                    if (lesQteFraisValides($lesFrais)) {
                        $pdo->majFraisForfait($visiteur, $leMois, $lesFrais);
                        
                        $lesCles = array_keys($lesFrais);
                        foreach ($lesCles as $unIdFrais) {
                            $qte = $lesFrais[$unIdFrais];
                            foreach ($montantFrais as $unfrais){
                                if($unIdFrais == $unfrais[0]){
                                    $totalF = $totalF +($lesFrais[$unIdFrais] *
                                        $unfrais[1]);
                                }
                            }
                            
                        }
                    } else {
                        ajouterErreur('Les valeurs des frais doivent être numériques');
                        include 'vues/v_erreurs.php';
                        $erreur = true;
                    }
                    foreach($lesHorsForfait as $fraisHorsForfait){
                        valideInfosFrais($fraisHorsForfait['dateFrais'], $fraisHorsForfait['libelle'], $fraisHorsForfait['montant']);
                        if (nbErreurs() != 0) {
                            include 'vues/v_erreurs.php';
                            $erreur = true;
                        } else {
                            $pdoc->majFraisHorsForfait( $fraisHorsForfait['id'], 
                                $fraisHorsForfait['dateFrais'],  
                                $fraisHorsForfait['montant'], 
                                $fraisHorsForfait['libelle']);
                            $totalHF =$totalHF + $fraisHorsForfait['montant'];
                        }
                    }
                    
                    if($erreur == false){
                        $pdoc->majValiderFicheFrais($visiteur, $leMois, ($totalF+$totalHF),$nbJustificatifs);
                        ajouterReussite("La modification a été prise en compte" );
                        include 'vues/v_success.php';
                    }
                    break;
            }
        }
        if(!empty($unFraisHorsForfait)){
            foreach($lesHorsForfait as $fraisHorsForfait){
                valideInfosFrais($fraisHorsForfait['dateFrais'], $fraisHorsForfait['libelle'], $fraisHorsForfait['montant']);
                if (nbErreurs() != 0) {
                    include 'vues/v_erreurs.php';
                } else {
                    $pdoc->majFraisHorsForfait( $unFraisHorsForfait, 
                        $lesHorsForfait[$unFraisHorsForfait]['dateFrais'],  
                        $lesHorsForfait[$unFraisHorsForfait]['montant'], 
                        $lesHorsForfait[$unFraisHorsForfait]['libelle']);
                }
            }
            ajouterReussite("La modification a été prise en compte");
            include 'vues/v_success.php';
        }
        if(!empty($suprimerFraisHorsForfait)){
            $mois = getMois(date('d/m/Y'));
            if ($pdo->estPremierFraisMois($visiteur, $mois)) {
                $pdo->creeNouvellesLignesFrais($visiteur, $mois);
            }
            $pdoc->majRefuserFraisHorsForfait($mois, $suprimerFraisHorsForfait);
            echo $mois;
        }
        
        $pdoc->majDateModifIdPerso($visiteur, $leMois, $idComptable);
        
        // élement du choix du visiteur et du mois
        $lesVisiteurs = $pdoc->getLesVisiteursCloture($uc);
        $visiteurASelectionner = $visiteur;
        $lesMois = $pdoc->getLesMoisDisponiblesCL($uc, $visiteur);
        $moisASelectionner = $leMois;
        $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($visiteur, $leMois);
        $libEtat = $lesInfosFicheFrais['libEtat'];
        include 'vues/v_listeVisiteurMois.php';
        
        
        $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($visiteur, $leMois);
        $lesFraisForfait = $pdo->getLesFraisForfait($visiteur, $leMois);
        $numAnnee = substr($leMois, 0, 4);
        $numMois = substr($leMois, 4, 2);
        
        include 'vues/v_ValiderAll.php';
        break;
}