<?php
/**
 * Vue Liste des mois
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Romain janz <cndromai@gamil.com>
 * @copyright no
 * @license   no
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */

$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
$idComptable = $_SESSION['id'];


switch ($action) {
    case 'selectionnerVisiteurs':
        /*recuper tous les visiteur de la base de donn� */
        $lesVisiteurs = $pdoc->getLesVisiteursCloture($uc);
        
        if(empty($visiteurASelectionner)){
            $visiteurASelectionner = $lesVisiteurs[0]['id'];
        }
        /*recuper tous les mois corespondant au visiteur*/
        $lesMois = $pdoc->getLesMoisDisponiblesCL($uc, $visiteurASelectionner);
        // Afin de sélectionner par défaut le dernier mois dans la zone de liste
        // on demande toutes les clés, et on prend la première,
        // les mois étant triés décroissants
        
        $lesCles = array_keys($lesMois);
        $moisASelectionner = $lesCles[0];
        //print_r($lesMois);
        include 'vues/v_listeVisiteurMois.php';
        break;
        
    case 'selectionnerMois':
        $lesVisiteurs = $pdoc->getLesVisiteursCloture($uc);
        
        $idVisiteur = filter_input(INPUT_POST, 'lstVisiteur', FILTER_SANITIZE_STRING);
        $visiteurASelectionner = $idVisiteur;
        
        /*recuper tous les mois corespondant au visiteur*/
        $lesMois = $pdoc->getLesMoisDisponiblesCL($uc, $idVisiteur);
        // Afin de sélectionner par défaut le dernier mois dans la zone de liste
        // on demande toutes les clés, et on prend la première,
        // les mois étant triés décroissants
        
        $lesCles = array_keys($lesMois);
        $moisASelectionner = $lesCles[0];
        //print_r($lesMois);
        include 'vues/v_listeVisiteurMois.php';
        break;
        
    case 'tryFraisForfait':
        $lesVisiteurs = $pdoc->getLesVisiteursCloture($uc);
        $visiteur = filter_input(INPUT_POST, 'lstVisiteur2', FILTER_SANITIZE_STRING);
        $visiteurASelectionner = $visiteur;
        $lesMois = $pdoc->getLesMoisDisponiblesCL($uc, $visiteur);
        $leMois = filter_input(INPUT_POST, 'lstMois', FILTER_SANITIZE_STRING);
        $moisASelectionner = $leMois;
        include 'vues/v_listeVisiteurMois.php';
        
        $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($visiteur, $leMois);
        $lesFraisForfait = $pdo->getLesFraisForfait($visiteur, $leMois);
        $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($visiteur, $leMois);
        $numAnnee = substr($leMois, 0, 4);
        $numMois = substr($leMois, 4, 2);
        $libEtat = $lesInfosFicheFrais['libEtat'];
        $montantValide = $lesInfosFicheFrais['montantValide'];
        $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
        $dateModif = dateAnglaisVersFrancais($lesInfosFicheFrais['dateModif']);
        include 'vues/v_etatFrais.php';
        include 'vues/v_validerPayement.php';
        break;
    case 'miseEnPayement':
        $visiteur = filter_input(INPUT_POST, 'lstVisiteur2', FILTER_SANITIZE_STRING);
        $leMois = filter_input(INPUT_POST, 'lstMois', FILTER_SANITIZE_STRING);
        print_r($visiteur);
        print_r($leMois);
        $pdoc->majRembourserFicheFrais($visiteur, $leMois);
        $pdoc->majDateModifIdPerso($visiteur, $leMois, $idComptable);
        ajouterReussite("Mise en paiement ");
        include 'vues/v_success.php';
        
        $lesVisiteurs = $pdoc->getLesVisiteursCloture($uc);
        $visiteurASelectionner = $visiteur;
        $lesMois = $pdoc->getLesMoisDisponiblesCL($uc, $visiteur);
        $moisASelectionner = $leMois;
        include 'vues/v_listeVisiteurMois.php';
            
        $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($visiteur, $leMois);
        $lesFraisForfait = $pdo->getLesFraisForfait($visiteur, $leMois);
        $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($visiteur, $leMois);
        $numAnnee = substr($leMois, 0, 4);
        $numMois = substr($leMois, 4, 2);
        $libEtat = $lesInfosFicheFrais['libEtat'];
        $montantValide = $lesInfosFicheFrais['montantValide'];
        $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
        $dateModif = dateAnglaisVersFrancais($lesInfosFicheFrais['dateModif']);
        include 'vues/v_etatFrais.php';
        include 'vues/v_validerPayement.php';
        break;

}