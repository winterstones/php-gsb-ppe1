<?php
/**
 * Vue Liste des visiteur et des mois
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Romain janz <cndromai@gamil.com>
 * @copyright no
 * @license   no
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte � Laboratoire GSB �
 */

?>
<div class="alert alert-success" role="alert">
    <?php
    foreach ($_REQUEST['reusssite'] as $reusssite) {
        echo '<p>' . htmlspecialchars($reusssite) . '</p>';
    }
    ?>
</div>