<?php
/**
 * Vue Liste des visiteur et des mois
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Romain janz <cndromai@gamil.com>
 * @copyright no
 * @license   no
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte � Laboratoire GSB �
 */

?>
<hr>
    <div class="row">
        <div class="panel panel-info">
            <div class="panel-heading">Descriptif des éléments hors forfait</div>
            <table class="table table-bordered table-responsive">
                <thead>
                    <tr>
                        <th class="date">Date</th>
                        
                        <th class="libelle">Libellé</th>  
                        <th class="montant">Montant</th>  
                        <th class="etat">Etat</th>
                        <th class="action">&nbsp;</th> 
                    </tr>
                </thead>  
                <tbody>
                <?php
                foreach ($lesFraisHorsForfait as $unFraisHorsForfait) {
                    $libelle = htmlspecialchars($unFraisHorsForfait['libelle']);
                    $date = $unFraisHorsForfait['date'];
                    $montant = $unFraisHorsForfait['montant'];
                    $refuser = $unFraisHorsForfait['refuser'];
                    $id = $unFraisHorsForfait['id']; 
                ?>           
                    <form action="index.php?uc=validerFrais&action=majFraisHorsForfait" method="post" role="form">
                    <input type="text" id="idVisiteur" name="idVisiteur" value="<?php echo $visiteur ?>"  class="form-control" style="display: none">
                  	<input type="text" id="mois" name="mois" value="<?php echo $leMois ?>"  class="form-control" style="display: none">
                  	<input type="text" id="unFraisHorsForfait" name="unFraisHorsForfait" value="<?php echo $id ?>"  class="form-control" style="display: none">
                        <tr>
                            <td>
                                <input type="text" id="txtDateHF" name="dateFrais" 
                                       class="form-control" id="text" value="<?php echo $date ?>">
                            </td>
                            <td>           
                                <input type="text" id="txtLibelleHF" name="libelle" 
                                       class="form-control" id="text" value="<?php echo $libelle ?>">
                            </td> 
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">€</span>
                                    <input type="text" id="txtMontantHF" name="montant" 
                                           class="form-control" value="<?php echo $montant ?>">
                                </div>
                            </td>
                            <td>
                            	<h4><?php echo $refuser ?></h4>
                            
                            
                            
                            
                            
                            </td>
                            <td>
                            <button class="btn btn-info" type="submit">Modifier</button>
                            
                            <input type="button"  class="btn btn-danger" value="Refuser" 
                            onclick="document.location='index.php?uc=validerFrais&action=refuserFraisHorsForfait&idFrais=<?php echo $id ?>&leMois=<?php echo $leMois ?>&visiteur=<?php echo $visiteur ?>'"/>
                            
                        </tr>
                    </form>
				<?php
                }
                ?>
			</tbody>  
            </table>
           	
        </div>
	</div>