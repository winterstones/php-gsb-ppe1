<?php
/**
 * Vue Liste des visiteur et des mois
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Romain janz <cndromai@gamil.com>
 * @copyright no
 * @license   no
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte � Laboratoire GSB �
 */

?>
<hr>
<form method="post" 
              action="index.php?uc=validerFrais&action=tryFraisForfaitSubmit" 
              role="form">
    <div class="row">    
        <h2>fiche de frais <?php echo $libEtat ?> du  
            <?php echo $numMois . '-' . $numAnnee ?>
        </h2>
        
        <h3>Eléments forfaitisés</h3>
        <div class="col-md-4">
                  <input type="text" id="idVisiteur" name="idVisiteur" value="<?php echo $visiteur ?>"  class="form-control" style="display: none">
                 <input type="text" id="mois" name="mois" value="<?php echo $leMois ?>"  class="form-control" style="display: none">
                 
                  
                <fieldset>       
                    <?php
                    foreach ($lesFraisForfait as $unFrais) {
                        $idFrais = $unFrais['idfrais'];
                        $libelle = htmlspecialchars($unFrais['libelle']);
                        $quantite = $unFrais['quantite']; ?>
                        <div class="form-group">
                            <label for="idFrais"><?php echo $libelle ?></label>
                            <input type="text" id="idFrais" 
                                   name="lesFrais[<?php echo $idFrais ?>]"
                                   size="10" maxlength="5" 
                                   value="<?php echo $quantite ?>" 
                                   class="form-control">
                        </div>
                        <?php
                    }
                    ?>
                    <button class="btn btn-info" type="submit" name="requete" value="MajFraisForfait">Modifier</button>
                </fieldset>
            
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="panel panel-info">
            <div class="panel-heading">Descriptif des éléments hors forfait</div>
            <table class="table table-bordered table-responsive">
                <thead>
                    <tr>
                        <th class="date">Date</th>
                        
                        <th class="libelle">Libellé</th>  
                        <th class="montant">Montant</th> 
                        <th class="action">&nbsp;</th> 
                    </tr>
                </thead>  
                <tbody>
                <?php
                foreach ($lesFraisHorsForfait as $unFraisHorsForfait) {
                    $libelle = htmlspecialchars($unFraisHorsForfait['libelle']);
                    $date = $unFraisHorsForfait['date'];
                    $montant = $unFraisHorsForfait['montant'];
                    $id = $unFraisHorsForfait['id']; 
                ?>           
                    
                        <tr>
                            <td>
                            <input type="text" id="unFraisHorsForfait" name="lesFraisHorsForfait[<?php echo $id ?>][id]" value="<?php echo $id ?>" style="display: none">
                                <input type="text" id="txtDateHF" name="lesFraisHorsForfait[<?php echo $id ?>][dateFrais]" 
                                       class="form-control" id="text" value="<?php echo $date ?>">
                            </td>
                            <td>           
                                <input type="text" id="txtLibelleHF" name="lesFraisHorsForfait[<?php echo $id ?>][libelle]" 
                                       class="form-control" id="text" value="<?php echo $libelle ?>">
                            </td> 
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">€</span>
                                    <input type="text" id="txtMontantHF" name="lesFraisHorsForfait[<?php echo $id ?>][montant]" 
                                           class="form-control" value="<?php echo $montant ?>">
                                </div>
                            </td>
                            
                            <td>
                            <button class="btn btn-info" type="submit" name="unFraisHorsForfait" value="<?php echo $id ?>">Modifier</button>
                            <button class="btn btn-danger" type="submit" name="suprimerFraisHorsForfait" value="<?php echo $id ?>">Suprimer</button>
                            
                            
                        </tr>
				<?php
                }
                ?>
			</tbody>  
            </table>
           	
        </div>
	</div>
	<div class="form-inline">Nb Justificatifs :  <input type="text" id="nbJustficatifs" name="justficatifs" 
         										class="form-control md-2" value="<?php echo $nbJustificatifs ?>" >	
	</div>
    <div>
    	<button class="btn btn-success" type="submit" name="requete" value="MajFicheForfait">Valider la fiche de frais</button>
    </div>
</form>