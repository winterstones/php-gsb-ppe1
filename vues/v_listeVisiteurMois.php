<?php
/**
 * Vue Liste des visiteur et des mois
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Romain janz <cndromai@gamil.com>
 * @copyright no
 * @license   no
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */
?>

<div class="row">
    <div class="col-md-1">
        <h4>Choisir : </h4>
    </div>
    <div class="col-md-4">
		<div class="form-inline">
			<form action="index.php?uc=<?php echo $uc;?>&action=selectionnerMois" name="myform" method="post" role="form">
            	<label for="lstVisiteur" accesskey="n">un visiteur : </label>
                <select id="lstVisiteur" name="lstVisiteur" class="form-control" onChange='myform.submit()'> 
                    <?php
                    foreach ($lesVisiteurs as $unVisiteur) {
                        $idVisiteur = $unVisiteur['id'];
                        $prenom = $unVisiteur['prenom'];
                        $nom = $unVisiteur['nom'];
                        if ($idVisiteur == $visiteurASelectionner) {
                            ?>
                            <option selected value="<?php echo $idVisiteur ?>">
                                <?php echo $prenom . ' ' . $nom ?> </option>
                            <?php
                        } else {
                            ?>
                            <option value="<?php echo $idVisiteur ?>">
                                <?php echo $prenom . ' ' . $nom ?> </option>
                            <?php
                        }
                    }
                    ?>
                 </select>
			</form>
		</div>
	</div>
    
	<div class="col-md-4">	
		<div class="form-inline">	
            <form action="index.php?uc=<?php echo $uc;?>&action=tryFraisForfait" name="mform"  method="post" role="form">
              

                <select id="lstVisiteur" name="lstVisiteur2" class="form-control"  style="display: none;">
                <option selected  value="<?php echo $visiteurASelectionner ?>"></option>
                </select>
                <label for="lstMois" accesskey="n">Mois : </label>
                <select id="lstMois" name="lstMois" class="form-control">
                    <?php
                    foreach ($lesMois as $unMois) {
                            $idVisiteur = $unMois['id'];
                            $mois = $unMois['mois'];
                            $numAnnee = $unMois['numAnnee'];
                            $numMois = $unMois['numMois'];
                            if ($mois == $moisASelectionner) {
                                ?>
                                <option selected value="<?php echo $mois ?>">
                                    <?php echo $numMois . '/' . $numAnnee ?> </option>
                                <?php
                            } else {
                                ?>
                                <option value="<?php echo $mois ?>">
                                    <?php echo $numMois . '/' . $numAnnee ?> </option>
                                <?php
                            }
                    }
                    ?>    
                </select>
            	<input id="ok" type="submit" value="Valider" class="btn btn-success" role="button">
			</form>
   			
		</div>
    </div>
</div>