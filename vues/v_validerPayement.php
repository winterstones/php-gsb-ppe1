<?php
/**
 * Vue Liste des visiteur et des mois
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Romain janz <cndromai@gamil.com>
 * @copyright no
 * @license   no
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */
?>
<div class="col-md-4">	
	<div class="form-inline">	
        <form action="index.php?uc=<?php echo $uc;?>&action=miseEnPayement" name="mform"  method="post" role="form">
          		
      		<select id="lstVisiteur" name="lstVisiteur2" class="form-control"  style="display: none;">
            	<option selected  value="<?php echo $visiteurASelectionner ?>"></option>
            </select>
			<select id="lstMois" name="lstMois" class="form-control"  style="display: none;">
                <option selected  value="<?php echo $moisASelectionner ?>"></option>
            </select>
            
            
        	<button class="btn btn-success" type="submit">Mise en paiement</button>
		</form>
		
	</div>
</div>